# Custom Lithium Batteries

Since 2008, Huizhou JB Battery Technology Limited has been designing and assembling custom battery solutions. We provide battery pack assemblies in all chemistries and for all electronic applications, all built to match our customers’ unique requirements. From medical devices and military equipment to emergency lighting systems and many industrial applications, we can design and manufacture the perfect portable power solution for your needs.

## Lithium-ion Battery
Lithium-ion battery ( Li-ion or LIB ) includes cylindrical steel shell and square aluminum shell. The anode materials include lithium cobalt carbonate, lithium manganese carbonate, lithium iron phosphate, nickel cobalt manganese and nickel cobalt aluminum.

## Ni-MH Battery
Nickel–metal hydride battery (NiMH or Ni-MH) is a type of rechargeable battery with good performance. It divides into high voltage and low voltage. NiMH have replaced NiCd for many roles, notably small rechargeable batteries.

## Lithium-Polymer Battery
Lithium-Polymer battery( LiPo or LIP ) is a rechargeable battery whose electrolyte is solid. Pack with Aluminum plastic film. Compared with Li-ion , it is higher energy density, more miniaturization, ultra-thin, lightweight, and high safety.

## LiFePO4 Battery
The lithium iron phosphate (LiFePO4) battery, also called LFP battery, is a type of rechargeable battery, specifically a lithium-ion battery, which uses LiFePO4 as a cathode material, and a graphitic carbon electrode with a metallic current collector grid as the anode.

##  Battery Applications
It’s widely used for consumer electronics, electronic toys, telecommunications devices, power tools, Lighting, surveying & mapping instruments, RFID products, and hybrid vehicle. Just tell us your requirements, PBS will evaluate & provide solution.

## Custom Made
According to different requirements, Li-ion, Li-po , Ni-Mh battery pack can be custom made. Consumer Electronics, Intelligent Household, Power Device (electric tools, toys, model aircraft, unmanned aerial vehicles (uavs), medical equipment, AGV robot, auto stop power supply).

## Contact Infomation

Email :  info@jbbatterychina.com

Website ;  https://www.lithiumbatterychina.com/ 